//
//  WINViewController.h
//  Nelson
//
//  Created by Richard George on 09/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PHIGeolocation.h"

@interface WINViewController : UIViewController <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager_;
    UIImageView *pointerImage_;
    CLLocation *location_;
}

- (void) enableLocationServices;
- (void) disableLocationServices;

@property (retain, nonatomic) IBOutlet UIImageView *pointerImage;
@property (nonatomic, retain) CLLocation *location;
@property (retain, nonatomic) CLLocationManager *locationManager;

@end
