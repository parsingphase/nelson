//
//  WINViewController.m
//  Nelson
//
//  Created by Richard George on 09/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "WINViewController.h"

@implementation WINViewController
@synthesize locationManager = locationManager_;
@synthesize pointerImage = pointerImage_;
@synthesize location = location_;


#pragma mark - location manager operations

- (void) enableLocationServices
{
    
    if(self.locationManager == nil) {
        self.locationManager = [[[CLLocationManager alloc] init] autorelease];
        
        
        // check if the hardware has a compass
        
        if ([CLLocationManager headingAvailable] == NO) {
            
            // No compass is available. This application cannot function without a compass, 
            // so a dialog will be displayed and no magnetic data will be measured.
            
            self.locationManager = nil;
            
            UIAlertView *noCompassAlert = [[UIAlertView alloc] initWithTitle:@"No Compass!" 
                                                                     message:@"This device does not have the ability to measure magnetic fields." 
                                                                    delegate:nil 
                                                           cancelButtonTitle:@"OK" 
                                                           otherButtonTitles:nil];
            
            [noCompassAlert show];
            [noCompassAlert release];
            
        } else {
            
            // heading service configuration
            self.locationManager.headingFilter = 1;
            
            // setup delegate callbacks
            self.locationManager.delegate = self;
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; //kCLLocationAccuracyNearestTenMeters;
            self.locationManager.distanceFilter= 10.0; //update frequency (distance)
            
            // start the compass
            [self.locationManager startUpdatingHeading];
            [self.locationManager startUpdatingLocation];
            
        }
    }
}

- (void) disableLocationServices
{
    if(self.locationManager != nil) {
        [self.locationManager stopUpdatingHeading];
        [self.locationManager stopUpdatingLocation];
        
        self.locationManager = nil;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //    self.magneticBearingLabel.text = @"LOADING";
    
    
    
    // setup the location manager
    [self enableLocationServices];
       
}

- (void)viewDidUnload
{
    [self disableLocationServices];
    
    [self setPointerImage:nil];
    [self setLocation:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - cleanup

- (void)dealloc {
    [locationManager_ release];
    [pointerImage_ release];
    [super dealloc];
}

#pragma mark - actions


#pragma mark - Location delegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)heading {
    
    double trafalgarBearing =0;
    
    if(self.location) {
        CLLocationCoordinate2D position=self.location.coordinate; 
        
        // update location display too
        //        self.locationLabel.text = [NSString stringWithFormat:@"%.5f,%.5f", position.latitude, position.longitude];
        
        CLLocationCoordinate2D TrafalgarSquare;
        TrafalgarSquare.latitude = 51.507774;
        TrafalgarSquare.longitude = -0.127947;
        
        trafalgarBearing = [PHIGeolocation greatCircleBearingFromLocation:position 
                                                               toLocation:TrafalgarSquare];
    }
    
    CLLocationDirection deviceBearingInRadians = M_PI*heading.trueHeading/180;
    self.pointerImage.transform = CGAffineTransformMakeRotation(2*M_PI + trafalgarBearing - deviceBearingInRadians);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.location= newLocation;
}

@end

