//
//  WINAppDelegate.h
//  Nelson
//
//  Created by Richard George on 09/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WINViewController;

@interface WINAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WINViewController *viewController;

@end

#define TESTFLIGHT_TEAM_TOKEN @"1d2e69a8ecab15202d81e7d9c4c4313b_NDY0MjUyMDExLTEyLTA5IDEzOjQ4OjA4LjQyNDg2OA"
